﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;


namespace firstPlugin
{

  
    /// <summary>
    /// Interaction logic for userControl.xaml
    /// </summary>
    public partial class userControl : Window
    {
        ExternalCommandData com;
        Document doc;
        
        public userControl(ExternalCommandData com)
        {
            this.doc = com.Application.ActiveUIDocument.Document;
            this.com = com;
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            
            FilteredElementCollector col = new FilteredElementCollector(com.Application.ActiveUIDocument.Document);
             col.GetElementCount();
            lblTekst.Content = col.GetElementCount()+5;

        }
    }
}
